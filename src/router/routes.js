export default [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),

    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'quiddity', component: () => import('pages/Quiddity.vue') },
      { path: 'quinacrines', component: () => import('pages/Quinacrines.vue') },
      { path: 'quinine', component: () => import('pages/Quinine.vue') },
      { path: 'quinquennia', component: () => import('pages/Quinquennia.vue') },
      { path: 'quinsy', component: () => import('pages/Quinsy.vue') },
      { path: 'quondam', component: () => import('pages/Quondam.vue') },
      { path: 'quidnunc', component: () => import('pages/Quidnunc.vue') }
    ]
  },

  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]
